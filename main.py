import weakref
import copy
import os
import pylibdmtx.pylibdmtx as dmtx
from PIL import Image, ImageDraw, ImageFont

class MissingPropertyError(Exception):
    pass

class PropertyError(Exception):
    pass

class StyleMeta(type):
    def __init__(cls, clsname, bases, _dict):
        cls._properties = {
        'width': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'height': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'background_color': {
            'test': cls.test_color,
            'inherit': True,
            'default': (255, 255, 255, 255)
        },
        'position': {
            'test': cls.test_position,
            'inherit': True,
            'default': 'relative',
        },
        'top': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'left': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'bottom': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'right': {
            'test': cls.test_int,
            'inherit': False,
            'default': None,
        },
        'justify_content': {
            'test': cls.test_tmb,
            'inherit': False,
            'default': 'flex-start',
        },
        'align_items': {
            'test': cls.test_tmb,
            'inherit': False,
            'default': 'flex-start',
        },
        'margin': {
            'test': cls.test_4side,
            'inherit': False,
            'default': (0, 0, 0, 0)
        },
        'border': {
            'test': cls.test_border,
            'inherit': False,
            'default': None,
        },
        'direction': {
            'test': cls.test_direction,
            'inherit': True,
            'default': 'row',
        },
        # TODO: implement test_font function ?
        #       wouldn't be needed if a possible OSError falls back
        #       gracefully. You may wanna implement fall-back logic
        #       on :class:TextComponent
        'font': {
            'test': lambda val: True,
            'inherit': True,
            'default': 'FreeSerif',
        },
        'font_size': {
            'test': cls.test_int,
            'inherit': True,
            'default': 15,
        },
        'font_color': {
            'test': cls.test_color,
            'inherit': True,
            'default': (0, 0, 0, 255)
        }
    }
    def test_size(cls, val):
        return isinstance(val, (tuple, list)) and len(val) == 2

    def test_color(cls, val):
        return isinstance(val, str) or \
                (
                    isinstance(val, (tuple, list)) and
                    len(val) in (3, 4) and
                    all(map(lambda el: el <= 255, val))
                )
    def test_position(cls, val):
        return val in ('absolute', 'relative', 'static')
    
    def test_int(cls, val):
        return isinstance(val, int)

    def test_tmb(cls, val):
        return val in ('flex_start', 'flex_end', 'center')
    
    def test_4side(cls, val):
        return isinstance(val, (tuple, list)) and len(val) == 4
    
    def test_border(cls, val):
        return True
    
    def test_direction(cls, val):
        return val in ('column', 'row')

class Props(object):
    def __init__(self, _dict={}):
        self.update(_dict)
    
    def update(self, _dict):
        self.__dict__.update(_dict)

    def __getattr__(self, attr):
        # make the :class:Style object deepcopiable
        # by adding the following conditional logic.
        if not attr.startswith('__'):
            return None
        else:
            raise AttributeError('%s is not defined' % attr)

    def __repr__(self):
        return '<%s.%s object with %s>' % (
            __name__,
            self.__class__.__name__,
            self.__dict__
        )

class Style(object):
    def __get__(self, obj, owner):
        if obj is None:
            return self
        else:
            return StyleHandler(obj)

class StyleHandler(metaclass=StyleMeta):
    def __init__(self, obj):
        self.__refobj__ = obj
    
    def __getattr__(self, attr):
        val = self._objstyle.get(attr)
        if not val is None:
            return val

        style_prop_conf = self._properties[attr]
        INHERITABLE = style_prop_conf['inherit']
        if INHERITABLE and not self.__refobj__._parent is None:
            return self.__refobj__._parent._style.__getattr__(attr)

        default = style_prop_conf['default']
        return default

    @property
    def _objstyle(self):
        try:
            refobj_style = self.__refobj__.__style__
        except AttributeError:
            refobj_style = self.__refobj__.__style__ = {}
        return refobj_style

    def _validate_style(self, _dict):
        for key, val in _dict.items():
            test_func = self._properties[key]['test']
            if not test_func(val):
                raise PropertyError('Wrong property value for "%s"' % key)
    
    def update(self, _dict):
        self._validate_style(_dict)
        self._objstyle.update(_dict)

    def __repr__(self):
        return '<%s.%s object with %s>' % (
            __name__,
            self.__class__.__name__,
            self._objstyle
        )

class ComponentMeta(type):
    _components = {}
    def __init__(cls, clsname, bases, _dict):
        if clsname in cls._components:
            raise AttributeError(
                'Image Compoment with the same name')
        cls._components[clsname] = cls
        super().__init__(clsname, bases, _dict)

class Component(metaclass=ComponentMeta):
    _style = Style()
    def __init__(self, style, **props):
        self._style.update(style)
        self.props = Props()
        self.props.update(props)
        self._children = None
        self.__parent = None
        self._id = None

    def init_image(self):
        self._image = Image.new(
            'RGBA',
            (self._style.width, self._style.height),
            self._style.background_color
        )
    @property
    def style(self):
        return self._style

    @property
    def _parent(self):
        if self.__parent is None:
            return self.__parent
        else:
            return self.__parent()

    @_parent.setter
    def _parent(self, val):
        self.__parent = val

    def set_style(self, style):
        self._style.update(style)
        # WARN: Old code
        # self.init_image()
        return self

    # WARN: Deprecated
    def _inject_parent_to(self, children):
        for i, el in enumerate(children):
            el.__parent = weakref.ref(self)
            el._id = i

    # WARN: Deprecated
    def _inject_siblings(self):
        for i, el in enumerate(self._children):
            siblings = list(self._children)
            siblings.pop(i)
            el._siblings = siblings
            el._sibling_index = i

    # Process the xy coordinate for indivisual child element
    # in the context of the area which wraps the whole siblings
    # The wrapping area's xy coordinate calculation, relative to a
    # parent image, will be better handled in the parent context.
    def _process_style(self):
        children = self._children
        if children is None:
            return

        style = self._style
        x, y = 0, 0
        if style.direction == 'row':
            for el in children:
                el_style = el._style
                if el_style.position != 'absolute':
                    x += el_style.margin[3]

                    left       = el_style.left
                    right      = el_style.right
                    top        = el_style.top
                    bottom     = el_style.bottom
                    margin_top = el_style.margin[0]

                    x_add = left if not left is None else \
                            right if not right is None else 0
                    y_add = top if not top is None else \
                            bottom if not bottom is None else 0

                    el._xy = (x + x_add, y + y_add + margin_top)
                    x += el_style.margin[1] + el_style.width
        
        elif style.direction == 'column':
            for el in children:
                el_style = el._style
                if el_style.position != 'absolute':
                    y += el_style.margin[0]

                    left        = el_style.left
                    right       = el_style.right
                    top         = el_style.top
                    bottom      = el_style.bottom
                    margin_left = el_style.margin[3]

                    x_add = left if not left is None else \
                            right if not right is None else 0
                    y_add = top if not top is None else \
                            bottom if not bottom is None else 0

                    el._xy = (x + x_add + margin_left, y + y_add)
                    y += el_style.margin[2] + el_style.height

    # TODO: Implement auto-sizing where component's size automatically
    #       expands to the extent that can have all its children
    #       endered within itself.
    def render(self):
        # init_image should never be located in the __init__ block
        # as a Component object will never know about its parent
        # at the time of instantiation,
        # and thus negates style inheritance feature.
        self.init_image()
        x_limit, y_limit = 0, 0
        x, y = 0, 0

        blank_image = Image.new(
            'RGBA',
            (self._style.width, self._style.height),
            (0, 0, 0, 0)
        )
        base_image = copy.copy(blank_image)
        if self._style.direction == 'row':
            for i, el in enumerate(self._children):
                el.render()
            # :method:_process_style should come after all children
            # components are rendered.
            # having this method at the top of :method:render will
            # disrupt components layout if some components are relying on
            # auto-sizing(width & height)
            self._process_style()

            for i, el in enumerate(self._children):
                temp_image = copy.copy(blank_image)
                temp_image.paste(el._image, el._xy)
                base_image = Image.alpha_composite(
                    base_image, temp_image
                )
                net_height = el._style.height + el._style.margin[0] + \
                            el._style.margin[2]
                y_limit = net_height if net_height > y_limit else y_limit
                if i == len(self._children) - 1:
                    x_limit = el._xy[0] + el._style.margin[1] + \
                                el._style.width

        elif self._style.direction == 'column':
            for i, el in enumerate(self._children):
                el.render()

            self._process_style()

            for i, el in enumerate(self._children):
                temp_image = copy.copy(blank_image)
                temp_image.paste(el._image, el._xy)
                base_image = Image.alpha_composite(
                    base_image, temp_image
                )
                net_width = el._style.width + el._style.margin[1] + \
                            el._style.margin[3]
                x_limit = net_width if net_width > x_limit else x_limit
                if i == len(self._children) - 1:
                    y_limit = el._xy[1] + el._style.margin[2] + \
                                el._style.height

        # TODO: align_items: cetner malfunction with multiple children
        if self._style.align_items == 'center':
            y = round((self._style.height - y_limit) / 2)
        elif self._style.align_items == 'flex_end':
            y = self._style.height - y_limit
        
        if self._style.justify_content == 'center':
            x = round((self._style.width - x_limit) / 2)
        if self._style.justify_content == 'flex_end':
            x = self._style.width - x_limit
            
        blank_image.paste(base_image, (x, y))
        self._image = Image.alpha_composite(self._image, blank_image)

    def _render(self):
        return False

    def show(self):
        self.render()
        self._image.show()

def create_element(comp, style, *children, **props):
    el = comp(style, **props)
    el._children = children
    # inject parent to children as weakref
    for i, child in enumerate(children):
        child._parent = weakref.ref(el)
        child._id = i
    return el

class TextComponent(Component):
    def __init__(self, style, **props):
        super().__init__(style, **props)
    
    @property
    def text(self):
        return self.props.text
    
    @text.setter
    def text(self, val):
        self.props.text = val

    # OO: Current method tries caching a TrueTypeFont object
    #     as a class property after its first initialization.
    #     Consider using cached_property to implement the
    #     same functionality in a more elegant way.
    # OO: Font caching in :class:TextComponent ?
    # TODO: Implement font-inheritance feature.
    @property
    def _font(self):
        try:
            return self.__font
        except AttributeError:
            font_name = self._style.font if \
                        not self._style.font is None else \
                        'FreeSerif' if \
                        os.name == 'posix' else \
                        'arial'
            font_size = self._style.font_size if \
                        not self._style.font_size is None else \
                        15
            self.__font = ImageFont.truetype(font_name, font_size)
            return self.__font

    @property
    def _font_color(self):
        return self._style.font_color if \
                not self._style.font_color is None else \
                (0, 0, 0, 255)

    def init_image(self):
        # OO: in a way of not calling self.textsize
        #     in case both height and width are declared
        width, height = self.textsize
        width = width if \
                self._style.width is None else \
                self._style.width
        height = height if \
                self._style.height is None else \
                self._style.height
        self._image = Image.new(
            'RGBA',
            (width, height),
            self._style.background_color
        )
        draw = ImageDraw.Draw(self._image)
        draw.text((0, 0), self.text, 
            fill=self._font_color, font=self._font)
        self.set_style({'width': width, 'height': height})

    @property
    def textsize(self):
        return self._get_textsize()

    def _get_textsize(self):
        return ImageDraw.ImageDraw.textsize(
            self, self.text, font=self._font, spacing=4, direction=None
        )
    # Interface to mock the ImageDraw.textsize function    
    def _multiline_check(self, text):
        return ImageDraw.ImageDraw._multiline_check(self, text)

class DMCComponent(Component):
    def init_image(self):
        super().init_image()
        data_string = _extract_data(self.props.data)
        dmtx_encoded = dmtx.encode(data_string.encode())
        DMC_image = Image.frombytes(
                        'RGB',
                        (dmtx_encoded.width, dmtx_encoded.height),
                        dmtx_encoded.pixels
                    )
        # resizing logic
        new_width = dmtx_encoded.width if \
                    dmtx_encoded.width > self._style.width else \
                    self._style.width
        new_height = dmtx_encoded.height if \
                    dmtx_encoded.height > self._style.height else \
                    self._style.height
        
        if self._image.size != (new_width, new_height):
            self.set_style({
                'width': new_width,
                'height': new_height,
            })
            self._image = self._image.resize((new_width, new_height))

        self._image.paste(DMC_image)

def _extract_data(obj):
    data_string = ''
    if isinstance(obj, dict):
        for val in obj.values():
            data_string += _extract_data(val)
    elif isinstance(obj, (list, tuple)):
        for val in obj:
            data_string += _extract_data(val)
    elif isinstance(obj, str):
        data_string += obj
    elif isinstance(obj, float):
        data_string += format(obj, '.2f')
    elif isinstance(obj, int):
        data_string += str(obj)
    else:
        raise TypeError('Not supported datatype %s' % type(obj))
    
    return data_string

def KGM_to_KG(ISO_unit):
    kg, g = ISO_unit.split('KGM')
    return str(int(kg) + int(g) / 1000) + 'KG'
    
if __name__ == '__main__':
    e = create_element
    st_main = {
        'width': 1000,
        'height': 430,
        'background_color': (255, 255, 255, 255),
        'position': 'relative',
        'direction': 'column',
        'margin': (0, 0, 0, 0),
    }
    layout_wrapper = {
        'width': 1000,
        'height': 430,
        'margin': (15, 0, 0, 30),
    }
    st_section_1 = {
        'width': 1000,
        'height': 270,
        'background_color': (0, 0, 0, 0),
        'position': 'relative',
        'direction': 'row',
        'margin': (0, 0, 0, 0),
    }
    st_section_2 = {
        'width': 950,
        'height': 130,
        #'background_color': (0, 255, 0, 255),
        'position': 'relative',
        'direction': 'row',
        'margin': (20, 0, 0, 10),
    }
    st_barcode = {
        'width': 0,
        'height': 0,
        'position': 'relative',
        'background_color': (0, 0, 0, 255),
        'margin': (0, 10, 0, 0),
    }
    st_part_info = {
        'width': 700,
        'height': 270,
        'position': 'relative',
        #'background_color': (255, 0, 0, 255),
        'direction': 'row',
        'margin': (0, 0, 0, 10),
    }
    st_part_info_sub = {
        'width': round(st_part_info['width'] / 4),
        'height': st_part_info['height'],
        'position': 'relative',
        #'background_color': (0, 255, 0, 255),
        'margin': (0, 0, 0, 0),
        'direction': 'column',
        'font': 'UbuntuMono-R',
        'font_size': 25,
    }
    st_part_info_sub_2 = {
        'width': 200,
        'height': 220,
        'position': 'relative',
        'direction': 'column',
        'font': 'UbuntuMono-R',
        'font_size': 25,
    }
    st_info_text = {
        'height': 26,
        #'position': 'relative',
        # 'background_color': (255, 255, 0, 255),
        #'direction': 'row',
        #'font_size': 25,
        #'font': 'UbuntuMono-R',
        'margin': (0, 0, 10, 0),
    }
    RoHS = {
        'font_size': 70,
        'font': 'FreeSerif',
        'top': 0,
        'left': -45,
    }
    st_info_text_b = dict(st_info_text, **{'font': 'UbuntuMono-B'})
    import json
    with open('DMC.data.json', 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
    DMC_data_string = _extract_data(data)

    DMC_label = \
        e(Component, st_main,
            e(Component, layout_wrapper, 
                e(Component, st_section_1,
                    e(DMCComponent, st_barcode, data=DMC_data_string),
                    e(Component, st_part_info,
                        e(Component, st_part_info_sub,
                            e(TextComponent, st_info_text_b, text='Part No     :'),
                            e(TextComponent, st_info_text_b, text='Ord. Code   :'),
                            e(TextComponent, st_info_text_b, text='Man Date    :'),
                            e(TextComponent, st_info_text_b, text='Exp. Date   :'),
                            e(TextComponent, st_info_text_b, text='Add. Info   :'),
                            e(TextComponent, st_info_text_b, text='Supplier ID :'),
                            e(TextComponent, st_info_text_b, text='Part Name   :'),
                        ),
                        e(Component, dict(st_part_info_sub, **{'width': 165}),
                            e(TextComponent, st_info_text, text=data['part_no']['val']),
                            e(TextComponent, st_info_text, text=data['ord_code']['val']),
                            e(TextComponent, st_info_text, text=data['man_date']['val']),
                            e(TextComponent, st_info_text, text=data['exp_date']['val']),
                            e(TextComponent, st_info_text, text=data['add_info']['val']),
                            e(TextComponent, st_info_text, text=data['supplier_id']['val']),
                            e(TextComponent, dict(st_info_text, **{'margin': (0,0,0,0)}), text='SOLDER BAR'),
                            e(TextComponent, st_info_text, text='HSE-02-B20'),
                        ),
                        e(Component, st_part_info_sub,
                            e(TextComponent, st_info_text_b, text='Quantity    :'),
                            e(TextComponent, st_info_text_b, text='Index       :'),
                            e(TextComponent, st_info_text_b, text='1.Batch     :'),
                            e(TextComponent, st_info_text_b, text='2.Batch     :'),
                            e(TextComponent, st_info_text_b, text='MS-Level    :'),
                            e(TextComponent, st_info_text_b, text='Package-ID  :'),
                        ),
                        e(Component, st_part_info_sub,
                            e(TextComponent, st_info_text, text=KGM_to_KG(data['quantity']['val'])),
                            e(TextComponent, st_info_text, text=data['index']['val']),
                            e(TextComponent, st_info_text, text=data['batch_no_1']['val']),
                            e(TextComponent, st_info_text, text=data['batch_no_2']['val']),
                            e(TextComponent, st_info_text, text=data['ms_level']['val']),
                            e(TextComponent, st_info_text, text=data['package_id']['val']),
                        ),
                    )
                ),
                e(Component, st_section_2,
                    e(Component, dict(st_part_info_sub_2, **{'font': 'UbuntuMono-B'}),
                        e(TextComponent, st_info_text, text='Supplier      :'),
                        e(TextComponent, st_info_text, text='Man. Loc      :'),
                        e(TextComponent, st_info_text, text='Man. Part No. :'),
                    ),
                    e(Component, st_part_info_sub_2,
                        e(TextComponent, st_info_text, text='SAESINSANGSA'),
                        e(TextComponent, st_info_text, text=data['man_loc']['val']),
                        e(TextComponent, st_info_text, text=data['man_part_no']['val']),
                    ),
                    e(Component, dict(st_part_info_sub_2, **{'font': 'UbuntuMono-B'}),
                        e(TextComponent, st_info_text, text='Purchase      :'),
                        e(TextComponent, st_info_text, text='Shipping Note :'),
                        e(TextComponent, st_info_text, text='Supplier Data :'),
                    ),
                    e(Component, st_part_info_sub_2,
                        e(TextComponent, st_info_text, text=data['purchase']['val']),
                        e(TextComponent, st_info_text, text=data['shipping_note']['val']),
                        e(TextComponent, st_info_text, text=data['supplier_data']['val']),
                    ),
                    e(TextComponent, RoHS, text='RoHS'),
                )
            )
        )
    DMC_label.render()
    DMC_label._image = DMC_label._image.convert('RGB')
    DMC_label._image.save(
        r'/home/point1304/projects/Bosch-DMC-Label/DMC_Label_Sample_SAESINSANGSA.jpg',
        'JPEG',
    )
    a4im = Image.new('RGB', (3088, 4367), (255, 255, 255))
    a4im.paste(DMC_label._image, (220, 220))
    a4im.save(
        r'./DMC_Label_Sample_SAESINSANGSA_a4.jpg',
        'JPEG',
    )
    a4im_x2 = Image.new('RGB', (1544, 2184), (255, 255, 255))
    a4im_x2.paste(DMC_label._image, (110, 110))
    a4im_x2.save(
        r'./DMC_Label_Sample_SAESINSANGSA_a4x2.jpg',
        'JPEG',
    )
    print(DMC_data_string)