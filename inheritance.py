class Style(object):
    def __get__(self, obj, owner):
        return StyleHandler(obj)

class StyleHandler(object):
    def __init__(self, obj):
        self.__refobj__ = obj
    
    def __getattr__(self, attr):
        if self._objstyle.get(attr) is None:
            if not self.__refobj__._parent is None:
                return self.__refobj__._parent._style.__getattr__(attr)
            else:
                raise AttributeError
        else:
            return self._objstyle.get(attr)

    @property
    def _objstyle(self):
        try:
            refobj_style = self.__refobj__.__style__
        except AttributeError:
            refobj_style = self.__refobj__.__style__ = {}
        return refobj_style
    
    def update(self, _dict):
        self._objstyle.update(_dict)

class Foo(object):
    _style = Style()
    def __init__(self):
        self._parent = None

if __name__ == '__main__':
    foo = Foo()
    sub_foo = Foo()
    foo._style.update({'submarine': 'yellow'})
    sub_foo._parent = foo
    print(sub_foo._style.submarine)
    print(foo._style.submarine)
    sub_foo._style.update({'submarine': 'blue'})
    print(sub_foo._style.submarine)
    print(foo._style.submarine)
